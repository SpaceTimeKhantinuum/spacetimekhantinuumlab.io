---
title: About me
subtitle: What should I put here?
comments: false
---

My name is Sebastian Khan. I am a gravitational-wave astronomer turned data scientist.

## Academic History

- 2019-2021: Research Fellow: Gravity Exploration Institute, School of Physics and Astronomy, Cardiff, Wales, UK
- 2016-2019: postdoctoral research associate: Max-Planck-Institut fur Gravitationsphysik, Albert-Einstein-Institut, Hannover, Germany
- 2012-2016: PhD: Cardiff University, School of Physics and Astronomy, Cardiff, Wales, UK
    - Thesis: [Numerical modelling of black-hole-binary mergers
](https://orca.cardiff.ac.uk/93841/)

My ORCID: [orcid.org/0000-0003-4953-5754](https://orcid.org/0000-0003-4953-5754)

## Interests

- Modelling
- Data Analysis
- Bayesian Inference
- Machine Learning
- Deep Learning
- AI
- My terrible [Dota buff](https://www.dotabuff.com/players/8583689) and [opendota](https://www.opendota.com/players/8583689) profiles
