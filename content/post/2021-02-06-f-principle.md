---
title: demonstrating the f-principle
date: 2021-02-06
tags: ["python", "machine learning", "deep learning", "artificial neural networks"]
---

# demonstrating the 'f-principle'

Here we try and demonstrate the f-principle, the tendency for ANNs for first fit low frequency data then high frequency.

There are quite a few papers on this which you can find by looking at this paper and papers it references and papers that
reference this: https://arxiv.org/abs/1901.06523

We also look at the multi-scale artificial neural network (M-scale ANN) from: https://arxiv.org/abs/1910.11710

We show that the M-scale ANN and drastically improve the performance of these ANNs for these regression tasks.

# imports


```python
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
mpl.rcParams.update({"font.size": 16})
```

here we use one of my libraries, pugna, to make building and training the networks easier.


```python
import pugna.layers
import pugna.activations
```


```python
import tensorflow as tf
```

## generate fake training data

here we generate some fake data, which is just a 1D time series composed of two frequencies.

wave 1 with a frequency of 1Hz and wave 2 with a frequency of 4 Hz.


```python
def gen_data(npoints=201):
    """
    example from https://github.com/xuzhiqin1990/F-Principle/blob/master/main_1d_20190924.py
    """
    x = np.linspace(-2, 2, npoints)[:,np.newaxis]

    f=1
    om = 2*np.pi*f
    y1 = np.sin(om*x)

    f=4
    om = 2*np.pi*f
    y2 = np.sin(om*x)

    y = y1+y2
    return x, y, y1, y2
```


```python
x, y, y1, y2 = gen_data(npoints=101)
```


```python
x.shape
```




    (101, 1)




```python
def compute_fft(x, y):
    """
    function to compute the fourier transform and sample frequencies.
    only keeping the positive side of the fft and normalising.
    """
    N = len(x)
    dt = x[1] - x[0]

    yf = 2.0/N * np.fft.fft(y)[:N//2]
    xf = np.fft.fftfreq(N, dt)[:N//2]

    return xf, yf
```


```python
xf, yf = compute_fft(x[:,0], y[:,0])
_, y1f = compute_fft(x[:,0], y1[:,0])
_, y2f = compute_fft(x[:,0], y2[:,0])
```


```python
plt.figure(figsize=(14, 4))
plt.plot(xf, np.abs(yf), lw=3)
plt.plot(xf, np.abs(y1f), ls='--')
plt.plot(xf, np.abs(y2f), ls='--')
plt.axvline(1, c='k', ls='--')
plt.axvline(4, c='k', ls='--')
plt.xlabel("f [Hz]")
plt.ylabel("|fft(y)|")
plt.title("fourier domain showing the peaks for 1 and 4 Hz")
```




    Text(0.5, 1.0, 'fourier domain showing the peaks for 1 and 4 Hz')




![image](/f-principle_files/f-principle_14_1.png)



```python
plt.plot(x, y1, label='y1')
plt.plot(x, y2, label='y2')
plt.plot(x, y, label='total')
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.title("time domain data - we will train a neural network to model the green curve")
plt.xlabel("t [s]")
```




    Text(0.5, 0, 't [s]')




![png](/f-principle_files/f-principle_15_1.png)



```python

```

this is a helper function to plot the training history


```python
def plot_history(history, labels=None):
    plt.figure()
    for i, h in enumerate(history):
        loss = h.history["loss"]
        if labels:
            plt.plot(range(len(loss)), loss, label=labels[i])
        else:
            plt.plot(range(len(loss)), loss)
    plt.yscale("log")
    plt.title('train')
    if labels:
        plt.legend(bbox_to_anchor=(1.05, 1))

    if "val_loss"  in history[0].history.keys():
        plt.figure()
        for h in history:
            loss = h.history["val_loss"]
            plt.plot(range(len(loss)), loss, ls='--')
        plt.yscale("log")
        plt.title('validation')
```

these lines allows us to use our sReLU and s2ReLU activations functions from the M-scale paper


```python
# https://datascience.stackexchange.com/questions/58884/how-to-create-custom-activation-functions-in-keras-tensorflow
from tensorflow.keras.utils import get_custom_objects
get_custom_objects().update({'srelu': tf.keras.layers.Activation(pugna.activations.sReLU)})
get_custom_objects().update({'s2relu': tf.keras.layers.Activation(pugna.activations.s2relu)})
```

main function to build and fit the ANNs


```python
def build_and_fit_model(
    x,
    y,
    verbose=False,
    batch_size=None,
    nscales=1,
    epochs=1000,
    units=300,
    lr=0.001,
    activation='srelu',
    validation_data=None,
    scale_name='linear',
    use_mscale=False
):
    if batch_size is None:
        batch_size = x.shape[0]

    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.InputLayer(input_shape=(1,)))
    if use_mscale:
        model.add(pugna.layers.Mscale(units, nscales, scale_name=scale_name))
    else:
        model.add(tf.keras.layers.Dense(units))
    if activation == 'srelu':
        model.add(tf.keras.layers.Activation(pugna.activations.sReLU))
    elif activation == 's2relu':
        model.add(tf.keras.layers.Activation(pugna.activations.s2relu))
    elif activation == 'relu':
        model.add(tf.keras.layers.Activation(tf.keras.activations.relu))
    elif activation == 'tanh':
        model.add(tf.keras.layers.Activation(tf.keras.activations.tanh))

    model.add(tf.keras.layers.Dense(units, activation=activation))
    model.add(tf.keras.layers.Dense(units, activation=activation))
    model.add(tf.keras.layers.Dense(units, activation=activation))
    model.add(tf.keras.layers.Dense(units, activation=activation))
    model.add(tf.keras.layers.Dense(1, activation="linear"))

    # optimizer = tf.keras.optimizers.Adam(lr)
    optimizer = tf.keras.optimizers.Adam(lr, amsgrad=True)
    #optimizer = tf.keras.optimizers.Adam(lr, amsgrad=True, clipnorm=0.5)
    #optimizer = tf.keras.optimizers.Adam(lr, amsgrad=True, clipvalue=5.0)
    #optimizer = tf.keras.optimizers.Nadam(lr)
    # optimizer = tf.keras.optimizers.SGD(lr)
    model.compile(loss="mse", optimizer=optimizer)

    history = model.fit(x, y, batch_size=batch_size, epochs=epochs, verbose=verbose, validation_data=validation_data)

    return history, model
```


```python

```

# standard dense layer

lets train the standard feed-forward fully connected ANN

we will repeat the training 10 times so that we can see how the
performance of the training changes and how stable it is.

In general, it is a good idea to repeat the training multiple times is possible.
Sometimes you can get lucky (or unlucky) with the training


```python
def lowest_loss(historys, models):
    losses = [history.history['loss'][-1] for history in historys]
    idx = np.argmin(losses)
    print(f"idx: {idx} had the lowest loss")
    return historys[idx], models[idx]
```


```python
%%time
ntries = 10
history1s = []
model1s = []

print("working")
for i in range(ntries):
    print(f"{i}", end=",")
    history, model = build_and_fit_model(x, y, activation='relu', epochs=100)

    history1s.append(history)
    model1s.append(model)
print("done")
```

    working
    0,1,2,3,4,5,6,7,8,9,done
    CPU times: user 12.2 s, sys: 1.23 s, total: 13.4 s
    Wall time: 5.79 s



```python
history1, model1 = lowest_loss(history1s, model1s)
```

    idx: 4 had the lowest loss



```python
plot_history(history1s)
```


![png](/f-principle_files/f-principle_29_0.png)



```python
yhat1 = model1.predict(x)
```


```python
plt.figure(figsize=(14, 7))
plt.plot(x, y, label='data')
plt.plot(x, yhat1, label='dense')
plt.plot(x, y1, label='y1 (low-f)', lw=2, c='k', ls='--')
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
```




    <matplotlib.legend.Legend at 0x16a05ae50>




![png](/f-principle_files/f-principle_31_1.png)



```python
_, yhat1f = compute_fft(x[:,0], yhat1[:,0])
```


```python
plt.figure(figsize=(14, 4))
plt.plot(xf, np.abs(yf), lw=3)
# plt.plot(xf, np.abs(y1f), ls='--')
# plt.plot(xf, np.abs(y2f), ls='--')

plt.plot(xf, np.abs(yhat1f), label='prediction')

# plt.axvline(1, c='k', ls='--')
# plt.axvline(4, c='k', ls='--')

plt.legend()
```




    <matplotlib.legend.Legend at 0x16a0da490>




![png](/f-principle_files/f-principle_33_1.png)


This is the fourier transform of our ANN prediction.

We see that the ANN prediction (in orange) peaks up the low frequency peak but doesn't pick up the higher frequency peak.

This is the f-principle in action.


```python

```


```python

```

# M-scale network

Now let's see if we can do better! Let's us an M-scale network!


```python
%%time
ntries = 10
history2s = []
model2s = []

print("working")
for i in range(ntries):
    print(f"{i}", end=",")
    history, model = build_and_fit_model(x, y, activation='s2relu', epochs=100, nscales=10, units=300, scale_name='linear', use_mscale=True)

    history2s.append(history)
    model2s.append(model)
print("done")
```

    working
    0,1,2,3,4,5,6,7,8,9,done
    CPU times: user 16.2 s, sys: 3.02 s, total: 19.2 s
    Wall time: 8.43 s



```python
history2, model2 = lowest_loss(history2s, model2s)
```

    idx: 4 had the lowest loss



```python
model2.layers[0].K.numpy()
```




    array([ 1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,
            1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,
            1.,  1.,  1.,  1.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,
            2.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,
            2.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,  3.,  3.,  3.,  3.,  3.,
            3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,
            3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  4.,
            4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,
            4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,
            4.,  4.,  4.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,
            5.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,
            5.,  5.,  5.,  5.,  5.,  5.,  5.,  6.,  6.,  6.,  6.,  6.,  6.,
            6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,
            6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,  7.,  7.,
            7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,
            7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,
            7.,  7.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,
            8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,
            8.,  8.,  8.,  8.,  8.,  8.,  9.,  9.,  9.,  9.,  9.,  9.,  9.,
            9.,  9.,  9.,  9.,  9.,  9.,  9.,  9.,  9.,  9.,  9.,  9.,  9.,
            9.,  9.,  9.,  9.,  9.,  9.,  9.,  9.,  9.,  9., 10., 10., 10.,
           10., 10., 10., 10., 10., 10., 10., 10., 10., 10., 10., 10., 10.,
           10., 10., 10., 10., 10., 10., 10., 10., 10., 10., 10., 10., 10.,
           10.], dtype=float32)




```python
np.unique(model2.layers[0].K.numpy())
```




    array([ 1.,  2.,  3.,  4.,  5.,  6.,  7.,  8.,  9., 10.], dtype=float32)




```python
plot_history(history2s)
```


![png](/f-principle_files/f-principle_43_0.png)



```python
yhat2 = model2.predict(x)
```


```python
plt.figure(figsize=(14, 7))
plt.plot(x, y, label='data')
plt.plot(x, yhat2, label='dense')
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
```




    <matplotlib.legend.Legend at 0x16f0c5890>




![png](/f-principle_files/f-principle_45_1.png)



```python

```


```python
_, yhat2f = compute_fft(x[:,0], yhat2[:,0])
```


```python
plt.figure(figsize=(14, 6))
plt.plot(xf, np.abs(yf), lw=3)
# plt.plot(xf, np.abs(y1f), ls='--')
# plt.plot(xf, np.abs(y2f), ls='--')

plt.plot(xf, np.abs(yhat1f), label='prediction')
plt.plot(xf, np.abs(yhat2f), label='prediction mscale')

# plt.axvline(1, c='k', ls='--')
# plt.axvline(4, c='k', ls='--')

plt.legend()
```




    <matplotlib.legend.Legend at 0x16f7f58d0>




![png](/f-principle_files/f-principle_48_1.png)


now in green is the m-scale prediction. We see that it has picked up the second peak.

The training loss plot below shows about an order of magnitude improvement for the M-scale network
over the dense network in the same time.


```python
plot_history([history1, history2], labels=['dense', 'mscale'])
```


![png](/f-principle_files/f-principle_51_0.png)



```python

```


```python

```


```python
plt.figure(figsize=(14, 7))
plt.plot(x, y, '-', label='data')
plt.plot(x, yhat1, 'o', label='dense')
plt.plot(x, yhat2, 'o', label='mscale')
plt.plot(x, y1, label='y1 (low-f)', lw=2, c='k', ls='--')
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
```




    <matplotlib.legend.Legend at 0x16fcc4910>




![png](/f-principle_files/f-principle_54_1.png)



```python
yres1 = tf.keras.losses.mse(y, yhat1)
yres2 = tf.keras.losses.mse(y, yhat2)
```


```python
plt.figure(figsize=(14, 7))
plt.plot(x, yres1, '-', label='dense')
plt.plot(x, yres2, '-', label='mscale')
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.yscale('log')
plt.title("MSE residuals")
```




    Text(0.5, 1.0, 'MSE residuals')




![png](/f-principle_files/f-principle_56_1.png)



```python

```


```python

```

# run for longer

let's re-run these tests for longer to see what happens

Can we just training for longer to obtain a better model?

# standard dense layer


```python
%%time
ntries = 10
history1s = []
model1s = []

print("working")
for i in range(ntries):
    print(f"{i}", end=",")
    history, model = build_and_fit_model(x, y, activation='relu', epochs=1000)

    history1s.append(history)
    model1s.append(model)
print("done")
```

    working
    0,1,2,3,4,5,6,7,8,9,done
    CPU times: user 1min 30s, sys: 9.72 s, total: 1min 39s
    Wall time: 29 s



```python
history1, model1 = lowest_loss(history1s, model1s)
```

    idx: 9 had the lowest loss



```python
plot_history(history1s)
```


![png](/f-principle_files/f-principle_64_0.png)



```python
yhat1 = model1.predict(x)
```


```python
plt.figure(figsize=(14, 7))
plt.plot(x, y, label='data')
plt.plot(x, yhat1, label='dense')
plt.plot(x, y1, label='y1 (low-f)', lw=2, c='k', ls='--')
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
```




    <matplotlib.legend.Legend at 0x174af6150>




![png](/f-principle_files/f-principle_66_1.png)



```python

```


```python

```

# M-scale network


```python
%%time
ntries = 10
history2s = []
model2s = []

print("working")
for i in range(ntries):
    print(f"{i}", end=",")
    history, model = build_and_fit_model(x, y, activation='s2relu', epochs=1000, nscales=10, units=300, scale_name='linear', use_mscale=True)

    history2s.append(history)
    model2s.append(model)
print("done")
```

    working
    0,1,2,3,4,5,6,7,8,9,done
    CPU times: user 2min, sys: 28 s, total: 2min 28s
    Wall time: 43.8 s



```python
history2, model2 = lowest_loss(history2s, model2s)
```

    idx: 8 had the lowest loss



```python
model2.layers[0].K.numpy()
```




    array([ 1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,
            1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,
            1.,  1.,  1.,  1.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,
            2.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,
            2.,  2.,  2.,  2.,  2.,  2.,  2.,  2.,  3.,  3.,  3.,  3.,  3.,
            3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,
            3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  3.,  4.,
            4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,
            4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,  4.,
            4.,  4.,  4.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,
            5.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,  5.,
            5.,  5.,  5.,  5.,  5.,  5.,  5.,  6.,  6.,  6.,  6.,  6.,  6.,
            6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,
            6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,  6.,  7.,  7.,
            7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,
            7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,  7.,
            7.,  7.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,
            8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,  8.,
            8.,  8.,  8.,  8.,  8.,  8.,  9.,  9.,  9.,  9.,  9.,  9.,  9.,
            9.,  9.,  9.,  9.,  9.,  9.,  9.,  9.,  9.,  9.,  9.,  9.,  9.,
            9.,  9.,  9.,  9.,  9.,  9.,  9.,  9.,  9.,  9., 10., 10., 10.,
           10., 10., 10., 10., 10., 10., 10., 10., 10., 10., 10., 10., 10.,
           10., 10., 10., 10., 10., 10., 10., 10., 10., 10., 10., 10., 10.,
           10.], dtype=float32)




```python
np.unique(model2.layers[0].K.numpy())
```




    array([ 1.,  2.,  3.,  4.,  5.,  6.,  7.,  8.,  9., 10.], dtype=float32)




```python
plot_history(history2s)
```


![png](/f-principle_files/f-principle_74_0.png)



```python
yhat2 = model2.predict(x)
```


```python
plt.figure(figsize=(14, 7))
plt.plot(x, y, label='data')
plt.plot(x, yhat2, label='dense')
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
```




    <matplotlib.legend.Legend at 0x16a1e0950>




![png](/f-principle_files/f-principle_76_1.png)



```python
_, yhat1f = compute_fft(x[:,0], yhat1[:,0])
```


```python
_, yhat2f = compute_fft(x[:,0], yhat2[:,0])
```


```python
plt.figure(figsize=(14, 6))
plt.plot(xf, np.abs(yf), lw=3)
# plt.plot(xf, np.abs(y1f), ls='--')
# plt.plot(xf, np.abs(y2f), ls='--')

plt.plot(xf, np.abs(yhat1f), label='prediction')

plt.plot(xf, np.abs(yhat2f), label='prediction mscale')

# plt.axvline(1, c='k', ls='--')
# plt.axvline(4, c='k', ls='--')

plt.legend()
```




    <matplotlib.legend.Legend at 0x174813610>




![png](/f-principle_files/f-principle_79_1.png)


We see that even after 1000 epochs (10 times longer training than before) that the standard dense network cannot hope to compete with the M-scale network.

The time and frequency domain plots look incredible. The M-scale network has an extremely high level of accuracy.


```python
plot_history([history1, history2], labels=['dense', 'mscale'])
```


![png](/f-principle_files/f-principle_81_0.png)



```python

```


```python

```


```python
plt.figure(figsize=(14, 7))
plt.plot(x, y, '-', label='data')
plt.plot(x, yhat1, 'o', label='dense')
plt.plot(x, yhat2, 'o', label='mscale')
plt.plot(x, y1, label='y1 (low-f)', lw=2, c='k', ls='--')
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
```




    <matplotlib.legend.Legend at 0x16bbd2e50>




![png](/f-principle_files/f-principle_84_1.png)



```python
yres1 = tf.keras.losses.mse(y, yhat1)
yres2 = tf.keras.losses.mse(y, yhat2)
```


```python
plt.figure(figsize=(14, 7))
plt.plot(x, yres1, '-', label='dense')
plt.plot(x, yres2, '-', label='mscale')
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.yscale('log')
plt.title("MSE residuals")
```




    Text(0.5, 1.0, 'MSE residuals')




![png](/f-principle_files/f-principle_86_1.png)



```python

```

As a final check lets check against some validation data.

We will just generate the training data at more points to achieve this


```python
x, y, y1, y2 = gen_data(npoints=1001)
```


```python

```


```python
yhat1 = model1.predict(x)
yhat2 = model2.predict(x)
```


```python

```


```python

```


```python
xf, yf = compute_fft(x[:,0], y[:,0])
_, yhat1f = compute_fft(x[:,0], yhat1[:,0])
_, yhat2f = compute_fft(x[:,0], yhat2[:,0])
```


```python
yres1 = tf.keras.losses.mse(y, yhat1)
yres2 = tf.keras.losses.mse(y, yhat2)
```


```python
plt.figure(figsize=(14, 6))
plt.plot(xf, np.abs(yf), lw=3)
# plt.plot(xf, np.abs(y1f), ls='--')
# plt.plot(xf, np.abs(y2f), ls='--')

plt.plot(xf, np.abs(yhat1f), label='prediction')

plt.plot(xf, np.abs(yhat2f), label='prediction mscale')

# plt.axvline(1, c='k', ls='--')
# plt.axvline(4, c='k', ls='--')

plt.legend()

plt.xlim(0, 10)
```




    (0.0, 10.0)




![png](/f-principle_files/f-principle_96_1.png)



```python
plt.figure(figsize=(14, 7))
plt.plot(x, y, '-', label='data')
plt.plot(x, yhat1, 'o', label='dense')
plt.plot(x, yhat2, 'o', label='mscale')
plt.plot(x, y1, label='y1 (low-f)', lw=2, c='k', ls='--')
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
```




    <matplotlib.legend.Legend at 0x174b6cc90>




![png](/f-principle_files/f-principle_97_1.png)



```python
plt.figure(figsize=(14, 7))
plt.plot(x, yres1, '-', label='dense')
plt.plot(x, yres2, '-', label='mscale')
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
plt.yscale('log')
plt.title("MSE residuals")
```




    Text(0.5, 1.0, 'MSE residuals')




![png](/f-principle_files/f-principle_98_1.png)


We are looking for any extra features, high frequency oscillations in the prediction. This can happen when the network overfits and the performance on unseen data can be poor.

In that case the model would not generalise well and would not be very useful.

This is not the case here though so things look great!


```python

```
