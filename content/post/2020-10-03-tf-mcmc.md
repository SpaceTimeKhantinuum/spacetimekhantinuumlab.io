---
title: MCMC with TensorFlow
date: 2020-10-03
tags: ["bayes"]
---

I was inspired by [Colin Carroll's blog](https://colindcarroll.com/),
specifically his post on using multiple MCMC chains not through
multiple processors but through
[vectorisation](https://colindcarroll.com/2019/08/18/very-parallel-mcmc-sampling/).
The example I show wouldn't have been possible without Colin's code for me to
copy so thanks!

I'm trying to get my head around basic MCMC still and also
TensorFlow/TensorFlow Probability so I have adapted Carroll's MCMC numpy code
above and translated the numpy/scipy code into TF and TFP code to create a very
basic MCMC sampler using TF and TFP
called [tf-minimc](https://gitlab.com/SpaceTimeKhantinuum/tf-minimc),
again based on [Colin Carroll's minimc](https://github.com/ColCarroll/minimc).
However, I didn't do any vectorisation parallelisation.

You can see an example how to use it in this [jupyter notebook](https://gitlab.com/SpaceTimeKhantinuum/tf-minimc/-/blob/master/example/mcmc-example.ipynb)

## Timings

| Code | Sampling Time (10000 iterations) |
| :------ |:--- |
| numpy | 156 ms |
| TF eager | 44.8 s |
| TF graph | 595 ms |
| TF XLA  | 81.5 ms |

We find that a compile TF graph with XLA can be ~2 times faster than the
numpy code. However, these numbers should be taken with a grain
of salt because there is a start-up cost associated with building the TF
graph first. Is there a way around this? I'm not sure...
