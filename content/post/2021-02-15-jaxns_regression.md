---
title: JAXNS - jax based nested sampling
date: 2021-02-15
tags: ["python", "bayesian", "jax", "nested sampling", "inference"]
---

# introduction

In this post I test out JAXNS - nested sampling implemented in JAX.

Paper: [arxiv.org:2012.15286](https://arxiv.org/abs/2012.15286)

Github: [github.com/Joshuaalbert/jaxns](https://github.com/Joshuaalbert/jaxns)

Thanks to Joshua Albert from JAXNS for helping me understand how to use JAXNS

UPDATE: For a more recent version of JAXNS see my [google colab notebook](https://drive.google.com/file/d/1icz9PTSL-JJX8ukC7rwVpfKEw_K6IKAX/view?usp=sharing)

# code

```python
!pip install git+http://github.com/Joshuaalbert/jaxns.git
```

    Collecting git+http://github.com/Joshuaalbert/jaxns.git
      Cloning http://github.com/Joshuaalbert/jaxns.git to /tmp/pip-req-build-60od1v_1
      Running command git clone -q http://github.com/Joshuaalbert/jaxns.git /tmp/pip-req-build-60od1v_1
    Requirement already satisfied (use --upgrade to upgrade): jaxns==0.0.1 from git+http://github.com/Joshuaalbert/jaxns.git in /usr/local/lib/python3.6/dist-packages
    Building wheels for collected packages: jaxns
      Building wheel for jaxns (setup.py) ... [?25l[?25hdone
      Created wheel for jaxns: filename=jaxns-0.0.1-cp36-none-any.whl size=124812 sha256=acb0d7281c45943ae3ff67e3ba19082ce0f3963560823f51efdad792e0428aaf
      Stored in directory: /tmp/pip-ephem-wheel-cache-qh2qnt36/wheels/5b/ff/5c/fe43b5a52fbd5c5eaaef2fcd00aac0d827cd434cb5a42403f0
    Successfully built jaxns



```python
from jaxns.nested_sampling import NestedSampler
from jaxns.prior_transforms import PriorChain, UniformPrior, NormalPrior, HalfLaplacePrior
from jaxns.plotting import plot_cornerplot, plot_diagnostics
from jax import random, jit
from jax import numpy as jnp


import matplotlib
import matplotlib.pyplot as plt
matplotlib.rcParams.update({"font.size": 16})
```


```python
def lnlike(x, data, a, b, c, d, sigma):
    """
    gaussian likelihood
    """

    yhat = model(x, a, b, c, d)
    N = x.size

    term1 = -0.5 * N * jnp.log(2. * jnp.pi * sigma**2)
    term2 =  -0.5/sigma**2 * jnp.sum((y - yhat)**2)

    return term1 + term2
```


```python
def model(x, a, b, c, d):
    """cubic function"""
    return a*x**3 + b*x**2 + c*x + d

def make_data(a=1, b=0, c=2, d=1, sigma=0.1):
    """generate some data with noise"""
    x = jnp.linspace(-3., 3, 100)[:, None]
    y = model(x, a, b, c, d) + sigma*random.normal(random.PRNGKey(0), shape=(100,1))
    return x, y
```


```python
"""true parameters"""
a_true = -4
b_true = 3
c_true = 25
d_true = 6
sigma_true = 5
```


```python
x, y = make_data(a_true, b_true, c_true, d_true, sigma_true)
```

    WARNING:absl:No GPU/TPU found, falling back to CPU. (Set TF_CPP_MIN_LOG_LEVEL=0 and rerun for more info.)



```python
N = x.size
```


```python
x.shape
```




    (100, 1)




```python
y.shape
```




    (100, 1)




```python
"""what does the data look like"""
plt.figure()
plt.plot(x, y, 'o')
plt.show()
```


![png](/jaxns_regression_files/jaxns_regression_9_0.png)



```python
"""example log-likelihood call"""
lnlike(x, y, 1, 0, 1, 0, 0.1)
```




    DeviceArray(-3996202.07286962, dtype=float64)




```python
"""setup priors"""
prior_a = UniformPrior('a', -20., 20.)
prior_b = UniformPrior('b', -20., 20.)
prior_c = UniformPrior('c', 0, 40)
prior_d = UniformPrior('d', -20., 20.)
prior_sigma = HalfLaplacePrior('sigma', 1)
```


```python
prior_chain = PriorChain().push(prior_a).push(prior_b).push(prior_c).push(prior_d).push(prior_sigma)
```


```python
prior_chain
```




    _sigma_b ~ (1,) : DeltaPrior([1])
    sigma ~ (1,) : HalfLaplacePrior(_sigma_b)
    _d_high ~ (1,) : DeltaPrior([20.])
    _d_low ~ (1,) : DeltaPrior([-20.])
    d ~ (1,) : UniformPrior(_d_low,_d_high)
    _c_high ~ (1,) : DeltaPrior([40])
    _c_low ~ (1,) : DeltaPrior([0])
    c ~ (1,) : UniformPrior(_c_low,_c_high)
    _b_high ~ (1,) : DeltaPrior([20.])
    _b_low ~ (1,) : DeltaPrior([-20.])
    b ~ (1,) : UniformPrior(_b_low,_b_high)
    _a_high ~ (1,) : DeltaPrior([20.])
    _a_low ~ (1,) : DeltaPrior([-20.])
    a ~ (1,) : UniformPrior(_a_low,_a_high)




```python
random.uniform(random.PRNGKey(234526), shape=(prior_chain.U_ndims,))
```




    DeviceArray([0.92468709, 0.40160492, 0.42350238, 0.84725093, 0.40543108],            dtype=float64)




```python
#prior chain acts on u ~ U^dim[0,1]
prior_chain(random.uniform(random.PRNGKey(234526), shape=(prior_chain.U_ndims,)))
```




    {'_a_high': DeviceArray([20.], dtype=float64),
     '_a_low': DeviceArray([-20.], dtype=float64),
     '_b_high': DeviceArray([20.], dtype=float64),
     '_b_low': DeviceArray([-20.], dtype=float64),
     '_c_high': DeviceArray([40], dtype=int64),
     '_c_low': DeviceArray([0], dtype=int64),
     '_d_high': DeviceArray([20.], dtype=float64),
     '_d_low': DeviceArray([-20.], dtype=float64),
     '_sigma_b': DeviceArray([1], dtype=int64),
     'a': DeviceArray([-3.7827568], dtype=float64),
     'b': DeviceArray([13.89003718], dtype=float64),
     'c': DeviceArray([16.94009526], dtype=float64),
     'd': DeviceArray([-3.9358031], dtype=float64),
     'sigma': DeviceArray([2.58610375], dtype=float64)}




```python
"""
I had to index the result, see the '[0]', in order for my 'log_likelihood'
function to work in 'NestedSampler'
"""
log_likelihood = lambda a, b, c, d, sigma, **unused_kwargs: lnlike(x, y, a, b, c, d, sigma)[0]
```


```python
# this doesn't work because of the [0] above
# log_likelihood(1, 0, 1, 1, 0.1)
```


```python
# test prior chain
prior_chain.test_prior(random.PRNGKey(2345264), 100, log_likelihood=log_likelihood)
```


```python
prior_chain(jnp.array([1,1,1,1,1]))
```




    {'_a_high': DeviceArray([20.], dtype=float64),
     '_a_low': DeviceArray([-20.], dtype=float64),
     '_b_high': DeviceArray([20.], dtype=float64),
     '_b_low': DeviceArray([-20.], dtype=float64),
     '_c_high': DeviceArray([40], dtype=int64),
     '_c_low': DeviceArray([0], dtype=int64),
     '_d_high': DeviceArray([20.], dtype=float64),
     '_d_low': DeviceArray([-20.], dtype=float64),
     '_sigma_b': DeviceArray([1], dtype=int64),
     'a': DeviceArray([20.], dtype=float64),
     'b': DeviceArray([20.], dtype=float64),
     'c': DeviceArray([40], dtype=int64),
     'd': DeviceArray([20.], dtype=float64),
     'sigma': DeviceArray([inf], dtype=float64)}




```python
log_likelihood(a=jnp.array([1]), b=jnp.array([0]), c=jnp.array([1]), d=jnp.array([1]), sigma=jnp.array([0.1]))

```




    DeviceArray(-3853288.23363534, dtype=float64)




```python
# but this works...correct
log_likelihood(**prior_chain(jnp.array([0.2,0.3,13.2,-2.,0.2])))
```




    DeviceArray(-8.36590225e+08, dtype=float64)




```python
ns = NestedSampler(log_likelihood, prior_chain, sampler_name='slice')
```


```python
@jit
def run(key):
    return ns(key=key,
              num_live_points=500,
              max_samples=1e5,
              collect_samples=True,
              termination_frac=0.01,
              sampler_kwargs=dict(depth=3, num_slices=5))
```


```python
%%time
# run with options (run twice to see the true speed without compile)
results = run(random.PRNGKey(2))
```

    CPU times: user 15.1 s, sys: 123 ms, total: 15.2 s
    Wall time: 15.2 s



```python
type(results)
```




    jaxns.nested_sampling.NestedSamplerResults




```python
plot_diagnostics(results)
plot_cornerplot(results)
```


![png](/jaxns_regression_files/jaxns_regression_26_0.png)



![png](/jaxns_regression_files/jaxns_regression_26_1.png)



```python
def plot_samples(data, true_value, title):
    plt.figure()
    plt.plot(data, 'o')
    plt.axhline(true_value, c='k', ls='--')
    plt.title(title)
    plt.show()
    plt.close()
```

Let's take a look at the samples. Hmm... they look strange!
That's because these are not the posterior samples. To get the posterior samples we have to re-weight them by the likelihood


```python
plot_samples(results.samples['a'][:,0], a_true, 'a')
plot_samples(results.samples['b'][:,0], b_true, 'b')
plot_samples(results.samples['c'][:,0], c_true, 'c')
plot_samples(results.samples['d'][:,0], d_true, 'd')
plot_samples(results.samples['sigma'][:,0], sigma_true, 'sigma')
```


![png](/jaxns_regression_files/jaxns_regression_29_0.png)



![png](/jaxns_regression_files/jaxns_regression_29_1.png)



![png](/jaxns_regression_files/jaxns_regression_29_2.png)



![png](/jaxns_regression_files/jaxns_regression_29_3.png)



![png](/jaxns_regression_files/jaxns_regression_29_4.png)


The re-weighting is done with the `resample` function


```python
from jaxns.utils import resample

samples = resample(random.PRNGKey(34252), results.samples, results.log_p, S=int(results.ESS))
```


```python
# so we asked for 100000 samples
results.samples['a'].shape
```




    (100000, 1)




```python
# and we end up with 3025 independent, posterior samples
samples['a'].shape
```




    (2957, 1)




```python

```


```python
plot_samples(samples['a'][:,0], a_true, 'a')
plot_samples(samples['b'][:,0], b_true, 'b')
plot_samples(samples['c'][:,0], c_true, 'c')
plot_samples(samples['d'][:,0], d_true, 'd')
plot_samples(samples['sigma'][:,0], sigma_true, 'sigma')
```


![png](/jaxns_regression_files/jaxns_regression_35_0.png)



![png](/jaxns_regression_files/jaxns_regression_35_1.png)



![png](/jaxns_regression_files/jaxns_regression_35_2.png)



![png](/jaxns_regression_files/jaxns_regression_35_3.png)



![png](/jaxns_regression_files/jaxns_regression_35_4.png)



```python
def plot_hist(data, true_value, title):
    plt.figure()
    plt.hist(data)
    plt.axvline(true_value, c='k', ls='--')
    plt.title(title)
    plt.show()
    plt.close()
```


```python

plot_hist(samples['a'][:,0], a_true, 'a')
plot_hist(samples['b'][:,0], b_true, 'b')
plot_hist(samples['c'][:,0], c_true, 'c')
plot_hist(samples['d'][:,0], d_true, 'd')
plot_hist(samples['sigma'][:,0], sigma_true, 'sigma')
```


![png](/jaxns_regression_files/jaxns_regression_37_0.png)



![png](/jaxns_regression_files/jaxns_regression_37_1.png)



![png](/jaxns_regression_files/jaxns_regression_37_2.png)



![png](/jaxns_regression_files/jaxns_regression_37_3.png)



![png](/jaxns_regression_files/jaxns_regression_37_4.png)



```python
def get_percentiles(data):
    return jnp.percentile(data, [5, 50, 95])
```


```python
# get percentiles
a_5, a_50, a_95 = get_percentiles(samples['a'])
b_5, b_50, b_95 = get_percentiles(samples['b'])
c_5, c_50, c_95 = get_percentiles(samples['c'])
d_5, d_50, d_95 = get_percentiles(samples['d'])
```


```python
print(a_5, a_50, a_95)
print(b_5, b_50, b_95)
print(c_5, c_50, c_95)
print(d_5, d_50, d_95)
```

    -4.2082343101501465 -4.003735065460205 -3.7925050258636475
    2.8067243099212646 3.1391470432281494 3.4612009525299072
    23.89307403564453 25.179162979125977 26.40064811706543
    3.8859715461730957 5.249072551727295 6.541553020477295



```python
plt.figure(figsize=(14, 8))
plt.plot(x, y, 'o', label='data')
plt.plot(x, model(x, a_50, b_50, c_50, d_50), c='k', label='median')

yhat_5 = model(x, a_5, b_5, c_5, d_5)
yhat_95 = model(x, a_95, b_95, c_95, d_95)

plt.fill_between(x[:,0], y1=yhat_5[:,0], y2=yhat_95[:,0], color='k', alpha=0.6, label='95% credible interval')


# plot random draws from the posterior
# i'm sure there is a better way to do this...
# max_samples = samples['a'].shape[0]
# n_draws = 10
# rand_index = jnp.unique(random.randint(random.PRNGKey(3), (n_draws,), 0, max_samples))
# for n in rand_index:
#     plt.plot(x, model(x, samples['a'][rand_index, 0], samples['b'][rand_index, 0], samples['c'][rand_index, 0], samples['d'][rand_index, 0]), color='k', alpha=0.1)

plt.xlabel('x')
plt.ylabel('y(x)')

plt.legend()
plt.show()
```


![png](/jaxns_regression_files/jaxns_regression_41_0.png)



```python

```
