---
title: Jupyter notebooks to markdown blog posts
date: 2020-10-08
tags: ["python"]
---

You can publish your jupyter notebooks to a personal website like this
by following a couple easy steps.

1. write notebook
2. convert notebook to markdown

    ```
    jupyter nbconvert --to markdown [NAME].ipynb
    ```

3. fix preamble

    You need to add the Hugo blog post metadata

    ```
    ---
    title: Jupyter notebooks to markdown blog posts
    date: 2020-10-08
    tags: ["python"]
    ---
    ```

4. Add images

    If you have any images the path in the markdown should be
    in the `static` directory. But then put the path
    of the image in the markdown without the `static`
    I found this out from [here](https://discourse.gohugo.io/t/solved-how-to-insert-image-in-my-post/1473)

    ```
    ![image](/image.png)
    ```

And that's it!
